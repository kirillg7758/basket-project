import React from "react";
import {Switch, Route} from "react-router-dom";
import Home from "../Home";
import Products from "../Products";

import "./Main.css";
import Sidebar from "../../../Sidebar/components/Sidebar";

const Main = () => {
    return(
        <main className="main">
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route path="/products">
                    <Products />
                </Route>
            </Switch>
            <Sidebar />
        </main>
    )
}

export default Main
