import {DECREASE_COUNT} from "../constants";

export const decreaseCountAction = (payload) => {
    return {
        type: DECREASE_COUNT,
        payload
    }
}
