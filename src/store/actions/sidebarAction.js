import {TOGGLE_BAR} from "../constants";

export const sidebarAction = (payload) => {
    return {
        type: TOGGLE_BAR,
        payload
    }
}
